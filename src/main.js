import Vue from 'vue';
import store from './store';
import * as plugins from './plugins';
import App from './app/index.vue';

Vue.config.productionTip = false;

export default new Vue({
  store,
  ...plugins,
  render: h => h(App),
}).$mount('#app');
