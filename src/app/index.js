import flatry from 'flatry';
import throttle from 'lodash.throttle';
import { mapActions, mapMutations, mapGetters } from 'vuex';
import { transitionEnd } from '@/utils';
import Icon from './components/icon';

export default {
  components: { Icon },
  data: () => ({
    isPending: false,
    isTouchDevice: false,
    currentPage: 0,
    perPage: 5,
    hasMorePages: true,

    viewportWidth: 0,
    spaceBetween: 20,
    visibleItemsCount: 3,

    // isHorizontalPanStarted: false,
    scrollLeft: 0,
    scrollLeftStart: 0,
    scrollToViewportEdgeTransition: false,
  }),
  computed: {
    ...mapGetters('votes', { votes: 'votesSortedByExpired' }),

    allowLoadVotes() {
      return this.hasMorePages && !this.isPending;
    },

    itemWidth() {
      const width =
        this.viewportWidth / this.visibleItemsCount -
        this.spaceBetween +
        this.spaceBetween / this.visibleItemsCount;

      /** но не меньше 300px */
      return Math.max(width, 300);
    },

    BASE_TLD: () => process.env.VUE_APP_BASE_TLD,

    MONTHS: () => [
      'января',
      'февраля',
      'марта',
      'апреля',
      'мая',
      'июня',
      'июля',
      'августа',
      'сентября',
      'октября',
      'ноября',
      'декабря',
    ],
  },
  watch: {
    // isTouchDevice: {
    //   once: true,
    //   handler() {
    //     console.log('isTouchDevice', this.isTouchDevice);
    //   },
    // },
    scrollLeft: throttle(function() {
      this.loadMoreIfNeeded();
    }, 50),
    votes() {
      this.$nextTick(() => this.loadMoreIfNeeded());
    },
  },
  methods: {
    ...mapActions('votes', ['fetchVotes']),
    ...mapMutations('votes', ['addVotes', 'reset']),

    // checkIsHorizontalPan(e) {
    //   return e.direction === 2 || e.direction === 4;
    // },

    getHumanDate(date) {
      const day = date.getDate();
      const month = this.MONTHS[date.getMonth()];

      return `${day} ${month}`;
    },

    setViewportWidth() {
      this.viewportWidth = this.$refs.viewport.offsetWidth;
    },
    setScrollLeft(e) {
      this.scrollLeft = this.scrollLeftStart - e.deltaX;
    },
    onPanStart(e) {
      // console.log('start', e);
      // this.isHorizontalPanStarted = this.checkIsHorizontalPan(e);
      // if (!this.isHorizontalPanStarted) return;

      this.setScrollLeft(e);
    },
    onPan(e) {
      // console.log('move', e);
      // if (!this.isHorizontalPanStarted) return;

      this.setScrollLeft(e);
    },
    onPanEnd(e) {
      // console.log('end', e);
      // if (!this.isHorizontalPanStarted) return;

      this.setScrollLeft(e);
      this.preventLinkClick(e);

      const { viewport: viewportNode, list: listNode } = this.$refs;

      let scrollToViewportLeftEdge = false;
      let scrollToViewportRightEdge = false;
      let scrollLeft = this.scrollLeft;
      if (scrollLeft < 0) {
        /** если отсвайпили далеко вправо, то надо прибить к левому краю вьюпорта */
        scrollLeft = 0;
        scrollToViewportLeftEdge = true;
      } else if (scrollLeft > 0) {
        const viewportClientWidth = viewportNode.clientWidth;
        const listScrollWidth = listNode.scrollWidth;

        /** а если далеко влево, то прибить к правому краю */
        if (scrollLeft + viewportClientWidth > listScrollWidth) {
          scrollLeft = listScrollWidth - viewportClientWidth;
          scrollToViewportRightEdge = true;
        }
      }

      this.scrollLeft = scrollLeft;
      this.scrollLeftStart = scrollLeft;

      /**
       * если расчёты показали, что список надо прибить к правому краю,
       * но при этом ещё возможны подгрузки, то прибивать не надо,
       * чтобы избежать лишних дёрганий. Для левого края это не актуально
       */
      if (scrollToViewportLeftEdge || (scrollToViewportRightEdge && !this.hasMorePages)) {
        /**
         * устанавливаем css-класс на списке,
         * который включает transition на transform.
         */
        this.scrollToViewportEdgeTransition = true;
        /** и ждём, пока список доедет к краю вьюпорта и убираем css-класс*/
        transitionEnd(listNode, () => (this.scrollToViewportEdgeTransition = false));
      }
    },

    preventLinkClick(event) {
      if (this.isTouchDevice) return;
      /**
       * Если список свайпили тянув за ссылку,
       * то надо отменить нативный клик по этой ссылке.
       * а то по ней сработает переход.
       *
       * Можно было бы использовать `event.target.closest('a')`,
       * чтобы найти ноду с ссылкой, но поддержка у этого метода не самая лучшая.
       */

      let linkNode;
      let node = event.target;
      while (node) {
        if (node.tagName === 'A') {
          linkNode = node;
          break;
        }
        node = node.parentElement;
      }

      if (!linkNode) return;

      linkNode.addEventListener('click', function listener(e) {
        e.preventDefault();
        linkNode.removeEventListener('click', listener);
      });
    },

    loadMoreIfNeeded() {
      if (!this.allowLoadVotes) return;

      /** здесь вычисляем - есть ли необходимость дозагрузить голосования */
      const { viewport: viewportNode, list: listNode } = this.$refs;
      const viewportClientWidth = viewportNode.clientWidth;
      const listScrollWidth = listNode.scrollWidth;

      /** пусть порог будет равен половине вьюпорта, но не меньше 300px */
      let threshold = Math.max(viewportClientWidth / 2, 300);
      /** подгружаем новую партию только если справа за вьюпортом осталось скроллить `threshold` пикселей */
      const loadMoreNeeded =
        listScrollWidth - viewportClientWidth - this.scrollLeft < threshold;

      loadMoreNeeded && this.loadMore();
    },

    async loadMore() {
      if (!this.allowLoadVotes) return;

      this.isPending = true;
      const params = { page: this.currentPage + 1, perPage: this.perPage };
      const [err, { votes, pagination } = {}] = await flatry(this.fetchVotes(params));
      this.isPending = false;
      if (err) return this.$toast.error(err.message);

      const { currentPage, perPage, hasMorePages } = pagination;
      this.currentPage = currentPage;
      this.perPage = perPage;
      this.hasMorePages = hasMorePages;
      this.addVotes(votes);
    },
  },
  beforeMount() {
    /** это 100%-й способ узнать - touch-устройство это или нет */
    const onFirstTouch = () => {
      this.isTouchDevice = true;
      window.removeEventListener('touchstart', onFirstTouch, false);
    };
    window.addEventListener('touchstart', onFirstTouch);

    /**
     * на ресайз окна тоже надо повесить обработчик,
     * чтобы дозагружать необходимое, если вьюпорт стал шире.
     * `onWindowResizeListener` класть в `data`-у нет необходимости
     */
    this.onWindowResizeListener = throttle(() => {
      this.setViewportWidth();
      this.$nextTick(() => this.loadMoreIfNeeded());
    }, 50);
    window.addEventListener('resize', this.onWindowResizeListener);
  },
  mounted() {
    this.setViewportWidth();
    this.loadMore();
  },
  beforeDestroy() {
    /**
     * `this.reset()` очищает стейт модуля в сторе.
     * Прямо сейчас в этом нет необходимости, но в реальном приложении
     * такой подход полезен при переходах на другие лэйауты,
     * чтобы не дрежать стейт этого модуля в памяти.
     */
    this.reset();

    /** и также необходимо почистить созданные обработчики */
    if (this.onWindowResizeListener) {
      window.removeEventListener('resize', this.onWindowResizeListener);
    }
  },
};
