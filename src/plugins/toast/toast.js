import { transitionEnd } from '@/utils';
import './index.scss';

class ToastCreator {
  constructor() {
    this.$root = null;
    this.hash = 'd9f4r271';
    this.classes = {
      container: `${this.hash}-toast-container`,
      toast: `${this.hash}-toast`,
      title: `${this.hash}-toast-title`,
      body: `${this.hash}-toast-body`,
      progress: `${this.hash}-toast-progress`,
      newline: `${this.hash}-toast-newline`,
      close: `${this.hash}-toast-close`,
      show: `${this.hash}-toast-show`,
      hide: `${this.hash}-toast-hide`,
    };

    this.addRoot();
    this.success = this.toastCreator('success');
    this.error = this.toastCreator('error');
    this.warn = this.toastCreator('warn');
  }

  addRoot() {
    this.$root = document.createElement('div');
    this.$root.classList.add(this.classes.container);
    document.body.appendChild(this.$root);
  }

  toastCreator(type = 'success') {
    return (message, title, params = {}) => {
      params.time = params.time || 8000;
      params.newline = params.newline || false;

      const localHash = `${message} ${title}`;
      const $toast = this.createToast(type, message, title, params);

      setTimeout(() => {
        this.showToast($toast);
        this.animateProgress($toast, params.time);
        this.handleClose($toast, localHash);
      }, 100);

      setTimeout(() => this.hideToast($toast), params.time);
    };
  }

  createToast(type, message, title, params) {
    const $toast = document.createElement('div');
    $toast.classList.add(this.classes.toast);
    $toast.classList.add(`${this.hash}-toast-${type}`);

    const newlineClass = params.newline ? this.classes.newline : '';
    const titleClasses = `${this.classes.title} ${newlineClass}`;
    const titleHtml = title ? `<b class="${titleClasses}">${title}</b>` : '';

    $toast.innerHTML = `
    <div class="${this.classes.body}">${titleHtml} ${message}</div>
    <div class="${this.classes.close}"><span>×</span></div>
    <div class="${this.classes.progress}"></div>`;
    this.$root.appendChild($toast);
    return $toast;
  }

  showToast($toast) {
    $toast.classList.add(this.classes.show);
  }

  animateProgress($toast, time) {
    const $progress = $toast.querySelector(`.${this.classes.progress}`);
    setTimeout(() => {
      const cssTime = `${time / 1000}s`;
      $progress.style.width = 0;
      $progress.style['transition-duration'] = cssTime;
      $progress.style['-webkit-transition-duration'] = cssTime;
      $progress.style['-o-transition-duration'] = cssTime;
    }, 100);
  }

  handleClose($toast) {
    const $close = $toast.querySelector(`.${this.classes.close}`);
    const closeFn = () => this.hideToast($toast);
    $close.addEventListener('click', closeFn, false);
  }

  hideToast($toast) {
    if (!$toast.parentElement) return;

    $toast.classList.remove(this.classes.show);
    $toast.classList.add(this.classes.hide);

    const removeFn = () => {
      $toast.parentElement.removeChild($toast);
    };
    transitionEnd($toast, removeFn, 400);
  }
}

export default ToastCreator;
