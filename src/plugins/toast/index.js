import Vue from 'vue';
import ToastCreator from './toast';

Vue.mixin({
  beforeCreate() {
    const options = this.$options;

    if (options.toast) {
      this.$toast = options.toast;
    } else if (options.parent && options.parent.$toast) {
      this.$toast = options.parent.$toast;
    }
  },
});

export default new ToastCreator();
