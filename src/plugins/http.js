import Vue from 'vue';
import axios from 'axios';

Vue.mixin({
  beforeCreate() {
    const options = this.$options;

    if (options.http) {
      this.$http = options.http;
    } else if (options.parent && options.parent.$http) {
      this.$http = options.parent.$http;
    }

    if (this.$store && !this.$store.$http) {
      this.$store.$http = this.$http;
    }
  },
});

const instance = axios.create({
  baseURL: process.env.VUE_APP_API_BASE_URL,
  headers: {
    Accept: 'application/json, text/plain, */*',
    'Content-Type': 'application/json',
  },
});

instance.interceptors.response.use(
  response => response.data.data,
  err => {
    /**
     * Бэкенд на ошибки валидации возвращает json-ответ с http-кодом >= 400 и < 500.
     * axios такие ответы выбрасывает как ошибку.
     * Поэтому такой ответ сохраним в отдельное поле `clientError`
     */

    if (err.response) {
      const { response } = err;
      const { status } = err.response;
      if (status && status < 500) {
        err.status = response.status;
        err.statusText = response.statusText;
        err.config = response.config;
        err.headers = response.headers;
        err.request = response.request;
        err.clientError = response.data.data;
      }
    }

    return Promise.reject(err);
  },
);

instance.interceptors.response.use(
  res => res,
  err => {
    const { request, response, clientError } = err;

    if (clientError) {
      /**
       * а уже здесь ответ от бэкенда запишем в инстанс ошибки,
       * чтобы иметь удобный доступ к json-ответу.
       */
      err.message = clientError.message;
      err.code = clientError.code;
      err.errors = clientError.errors;
      err.data = clientError.data;
    } else if (response) {
      err.message = response.statusText;
      err.code = response.status;
    } else if (request) {
      err.message =
        'Ошибка запроса. Отключите блокировщик рекламы, если он есть, и проверьте подключение к интернету.';
    }

    return Promise.reject(err);
  },
);

export default instance;
