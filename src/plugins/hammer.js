import Vue from 'vue';
import { VueHammer } from 'vue2-hammer';
VueHammer.config.pan = {
  threshold: 10,
  domEvents: true,
};

Vue.use(VueHammer);
