import defaultState from './state';

export default {
  reset(state) {
    const defaults = defaultState();
    Object.keys(state).forEach(key => (state[key] = defaults[key]));
  },

  addVotes({ votes }, data = []) {
    /**
     * если вдруг что-то пойдёт не так и с бэкенда прилетят объекты,
     * которые уже есть в списке (такое бывает при сильно динамичных пагинированных списках),
     * то дубликатов в массиве не будет, а уже имеющиеся элементы просто обновятся новыми данными.
     *
     * Такой подход прямо сейчас необязателен - просто привычка перестраховываться.
     */
    [].concat(data).forEach(item => {
      const idx = votes.findIndex(vote => vote.id === item.id);
      /** т.е. если элемент уже есть в списке - то обновляем его, если нет - добавляем */
      idx >= 0 ? votes.splice(idx, 1, { ...votes[idx], ...item }) : votes.push(item);
    });
  },
};
