export default {
  votesSortedByExpired({ votes }) {
    return [...votes].sort((a, b) => b.date - a.date);
  },
};
