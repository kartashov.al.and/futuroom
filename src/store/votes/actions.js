import flatry from 'flatry';
import { normalizeFetchedVote, normalizeFetchedPagination } from './utils';

export default {
  async fetchVotes(store, params = {}) {
    const { expired = true, perPage = 6, page = 1 } = params;

    const [err, { votes, pagination } = {}] = await flatry(
      this.$http('votes', {
        params: { expired, perPage, page },
      }),
    );
    if (err) throw err;

    return {
      votes: votes.map(v => normalizeFetchedVote(v)),
      pagination: normalizeFetchedPagination(pagination),
    };
  },
};
