/**
 * @param {Object} vote
 * @returns {Object}
 */
export function normalizeFetchedVote(vote) {
  const { date, expire } = vote;
  return {
    ...vote,
    date: new Date(date),
    expire: new Date(expire),
  };
}

/**
 * @param {Object} pagination
 * @returns {Object}
 */
export function normalizeFetchedPagination(pagination) {
  const { perPage } = pagination;
  return {
    ...pagination,
    perPage: parseInt(perPage, 10),
  };
}
